package Model.Ejercicio3;

public class Videojuegos implements Entregable {

    public static final int HORAS_ESTIMADAS_DEFAULT = 10;

    private String titulo = "", genero = "", compañia = "";
    private int horasEstimadas = HORAS_ESTIMADAS_DEFAULT;
    private boolean entregado = false;

    public Videojuegos() {
    }

    public Videojuegos(String titulo, int horasEstimadas) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
    }

    public Videojuegos(String titulo, String genero, String compañia, int horasEstimadas) {
        this.titulo = titulo;
        this.genero = genero;
        this.compañia = compañia;
        this.horasEstimadas = horasEstimadas;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getGenero() {
        return genero;
    }

    public String getCompañia() {
        return compañia;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }

    public void setHorasEstimadas(int horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    @Override
    public String toString() {
        return "Videojuegos{" +
                "titulo='" + titulo + '\'' +
                ", genero='" + genero + '\'' +
                ", compañia='" + compañia + '\'' +
                ", horasEstimadas=" + horasEstimadas +
                ", entregado=" + entregado +
                '}';
    }

    @Override
    public boolean entregar() {
        return this.entregado = true;
    }

    @Override
    public boolean devolver() {
        return this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return this.entregado;
    }

    @Override
    public int compareTo(Object a) {
        return 0;
    }
}
