package Model.Ejercicio3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

//        SERIE
//        private String titulo = "", genero = "", creador = "";
//        private int nroTemporadas = 3;
//        private boolean entregado = false;

        List<Serie> series = new ArrayList();

        Serie serie1 = new Serie();
        serie1.setTitulo("Dead Walking Dead");
        serie1.setGenero("Terror");
        serie1.setCreador("NA");
        serie1.setNroTemporadas(10);

        Serie serie2 = new Serie();
        serie2.setTitulo("Moon knight");
        serie2.setGenero("Fantasia");
        serie2.setCreador("NA2");
        serie2.setNroTemporadas(300);
        serie2.entregar();

        Serie serie3 = new Serie();
        serie3.setTitulo("The Witcher");
        serie3.setGenero("Fantasia");
        serie3.setCreador("NA3");

        Serie serie4 = new Serie();
        serie4.setTitulo("Prince Fresh");
        serie4.setGenero("Comedia");
        serie4.setCreador("NA4");
        serie4.setNroTemporadas(300);
        serie4.entregar();


        series.add(serie1);
        series.add(serie2);
        series.add(serie3);
        series.add(serie4);


//        private String titulo = "", genero = "", compañia = "";
//        private int horasEstimadas = 10;
//        private boolean entregado = false;

        List<Videojuegos> videojuegos = new ArrayList();

        Videojuegos videojuegos1 = new Videojuegos();
        videojuegos1.setTitulo("PUBG");
        videojuegos1.setGenero("Shooter");
        videojuegos1.setCompañia("Krafton");

        Videojuegos videojuegos2 = new Videojuegos();
        videojuegos2.setTitulo("WOW");
        videojuegos2.setGenero("MMORPG");
        videojuegos2.setCompañia("Blizzard");
        videojuegos2.setHorasEstimadas(200);
        videojuegos2.entregar();

        Videojuegos videojuegos3 = new Videojuegos();
        videojuegos3.setTitulo("DOTA2");
        videojuegos3.setGenero("MOBA");
        videojuegos3.setCompañia("valve");
        videojuegos3.setHorasEstimadas(1000);
        videojuegos3.entregar();


        videojuegos.add(videojuegos1);
        videojuegos.add(videojuegos2);
        videojuegos.add(videojuegos3);

        System.out.println("Listas");
        System.out.println(series);
         System.out.println(videojuegos);

        System.out.println("");
        //vevolver los videojuegos entregados
        System.out.println("vevolver los videojuegos entregados");
        System.out.println(series.stream().filter(serie -> serie.isEntregado()).collect(Collectors.toList()));
        System.out.println("Nro de series entregados " + series.stream().filter(serie -> serie.isEntregado()).count());

        System.out.println(videojuegos.stream().filter(videojuego -> videojuego.isEntregado()).collect(Collectors.toList()));
        System.out.println("Nro de series entregados " + videojuegos.stream().filter(videojuego -> videojuego.isEntregado()).count());


        //Encontrar los videojuegos con más horas y series con más temporadas
        System.out.println("");
        System.out.println("Encontrar los videojuegos con más horas y series con más temporadas");
//        series.sort(Comparator.comparing(Serie::getNroTemporadas).reversed());
//        videojuegos.sort(Comparator.comparing(Videojuegos::getHorasEstimadas).reversed());
//
//        System.out.println(series.stream().findFirst());
//        System.out.println(videojuegos.stream().findFirst());


        //¿Qué pasa si una serie tiene las mismas temporadas?
        int maxSerie = (series.stream().mapToInt( v -> v.getNroTemporadas()).max()).orElse(Serie.NUMERO_TEMPORADAS_DEFAULT);
//        System.out.println(maxSerie);
        System.out.println(series.stream().filter(serie -> serie.getNroTemporadas()==maxSerie).collect(Collectors.toList()));

        int maxVideojuegos = (videojuegos.stream().mapToInt( v -> v.getHorasEstimadas()).max()).orElse(Videojuegos.HORAS_ESTIMADAS_DEFAULT);
//        System.out.println(maxVideojuegos);
        System.out.println(videojuegos.stream().filter(videojuego -> videojuego.getHorasEstimadas()==maxVideojuegos).collect(Collectors.toList()));


    }
}
