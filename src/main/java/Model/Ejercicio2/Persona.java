package Model.Ejercicio2;

public class Persona {

    //constantes
    private final static String SEXO_DEFAULT = "H";
    private final static int INFRAPESO = -1;
    private final static int PESO_IDEAL = 0;
    private final static int SOBREPESO = 1;

    //variables
    private String nombre = "", apellidos = "", dni, sexo = SEXO_DEFAULT;
    private int edad = 0;
    private double peso = 0, altura = 0;

    public Persona() {
    }

    public Persona(String nombre, String sexo, int edad) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.edad = edad;
    }

    public Persona(String nombre, String apellidos, String dni, String sexo, int edad, double peso, double altura) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.sexo = sexo;
        this.edad = edad;
        this.peso = peso;
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    //    https://www.cdc.gov/healthyweight/spanish/assessing/bmi/adult_bmi/metric_bmi_calculator/bmi_calculator.html
    public int calcularIMC() {
        double imc = (this.peso / Math.pow(this.altura, 2));
        return imc < 20 ? INFRAPESO : imc >= 20 && imc <= 25 ? PESO_IDEAL : SOBREPESO;
    }

    public boolean esMayorDeEdad() {
        return this.edad >= 18 ? true : false;
    }

    private void comprobarSexo(String sexo) {
        this.sexo = sexo != "H" && sexo != "M" ? SEXO_DEFAULT : sexo;
    }

    @Override
    public String toString() {
        comprobarSexo(sexo);

        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", dni='" + dni + '\'' +
                ", sexo='" + sexo + '\'' +
                ", edad=" + edad +
                ", peso=" + peso +
                ", altura=" + altura +
                '}';
    }
}
